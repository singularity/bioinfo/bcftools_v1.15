# bcftools v1.15 Singularity container using conda and multi-stage build

BCFtools is a set of utilities that manipulate variant calls in the Variant Call Format (VCF) and its binary counterpart BCF. All commands work transparently with both VCFs and BCFs, both uncompressed and BGZF-compressed.


[bcftools site](http://samtools.github.io/bcftools/)


Based on debian 9.8 slim<br>
Singularity container based on the recipe: Singularity.bcftools_v1.15<br>
bcftools v1.15 package installation using Miniconda3 python 3.9  <br>


## Singularity Multi-stage build
https://sylabs.io/guides/3.5/user-guide/definition_files.html#multi-stage-builds

### 1) from docker image miniconda3
Without os and miniconda fixed verions, not ideal for reproductibility

### 2) from debian
Uses Singularity multi-stage build (redicinng image size))
1) Stage 1: Using debian 9.8-slim Minniconnda3 v4.11.0 installation, bcftools bioconda installation & conda pack
2) Stage 2: Using debian 9.8-slim get the "unpacked" bcftools in /opt/condapkg/bin

Definnition file: Singularity.bcftools_v1.15

### 3) from a lacally pre-built debian/miniconda image
Using pre-built image for the stage 1 reduces the build time and simplify the def file<br>
https://forgemia.inra.fr/singularity/miniconda3_py39_4.11.0

Definnition file: Singularity.bcftools_v1.15.pre-built_conda_image

### 4) From a pre-built debian/miniconda image available from oras
Using pre-built image for the stage 1 reduces the build time (~50%) and simplify the def file<br>
usinng oras we don't need to get the pre-built image for the stage 1

Definnition file: Singularity.bcftools_v1.15.oras

https://forgemia.inra.fr/singularity/miniconda3_py39_4.11.0<br>
oras:<br>
registry.forgemia.inra.fr/singularity/miniconda3_py39_4.11.0/miniconda3_py39_4.11.0:latest


##
(Image reduction from 280Mo to 77Mo)

usage:<br>
     bcftools_v1.15.sif --help


Local build:
```bash
sudo singularity build bcftools_v1.15.sif Singularity.bcftools_v1.15
```


image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>
can be pull (singularity version >=3.3) with:<br>
```bash
singularity pull bcftools_v1.15.sif oras://registry.forgemia.inra.fr/singularity/bcftools_v1.15/bcftools_v1.15:latest
```

contact: jacques.lagnel@inrae.fr

